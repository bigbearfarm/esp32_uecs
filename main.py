import machine, onewire, ds18x20
import usocket as socket
import esp32 ,math ,ujson
from sht31 import SHT31_Sensor
from hx711 import HX711
from utime import ticks_ms, ticks_diff, sleep, sleep_ms

#if machine.reset_cause() == machine.DEEPSLEEP_RESET:
#    print('woke from a deep sleep')
wdt = machine.WDT(timeout=300000)  # 5 minutes 
##################################################
# set parameter 
##################################################
# json config file read 
json_open = open('esp32_uecs.json', 'r')
json_load = ujson.load(json_open)

# CCM
CCM_name = str(json_load['CCM']['name'])
CCM_type = str(json_load['CCM']['type'])
CCM_room = str(json_load['CCM']['room'])
CCM_region = str(json_load['CCM']['region'])
CCM_order = str(json_load['CCM']['order'])
CCM_priority = str(json_load['CCM']['priority'])
CCM_ip = str(json_load['CCM']['ip_address'])

# ONEWIRE
if json_load['ONEWIRE']['load']:
  ds_pin = machine.Pin(json_load['ONEWIRE']['ds_pin'])
  ONEWIRE_Multiple = json_load['ONEWIRE']['multiple_Sensor'] 
  timer0 = machine.Timer(0)
  ms0 = json_load['ONEWIRE']['interval_second']*1000
  while True:
    ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
    roms = ds_sensor.scan()
    if len(roms) > 0:
      print('Found DS devices: ', roms)
      break
    else:
      print('ds_sensor.scaning......')
    sleep_ms(750)

# SHT31
if json_load['SHT31']['load']:
  sda = json_load['SHT31']['sda']
  scl = json_load['SHT31']['scl']
  timer1 = machine.Timer(1)
  ms1 = json_load['SHT31']['interval_second']*1000
  while True:
    sht31 = SHT31_Sensor(freq = 100000,sdapin = sda, sclpin = scl)
    if sht31 is not None:
      break
    else:
      print('sht31_sensor.scaning......')

# hx711    
if json_load['HX711']['load']:
  DOUT_PIN = json_load['HX711']['dout']
  SCK_PIN = json_load['HX711']['sck']
  hx711_name = json_load['HX711']['name']
  timer2 = machine.Timer(2)
  ms2 = json_load['HX711']['interval_second']*1000
  hx = HX711(DOUT_PIN, SCK_PIN)
  hx.set_scale(json_load['HX711']['set_scale'])
  if json_load['HX711']['tare']:
    hx.tare()

##################################################
# UECS UDP
##################################################
HOST = ''   
PORT = 16520
s =socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
ADDRESS = "255.255.255.255"

def CCM_send(name,val):
    msg = "<?xml version=\"1.0\"?><UECS ver=\"1.00-E10\">"
    msg += "<DATA type=\"" + CCM_name + name + "." + CCM_type + "\""
    msg += " room=" + "\"" + CCM_room + "\""
    msg += " region=" + "\"" +  CCM_region + "\""
    msg += " order=" + "\"" +  CCM_order + "\""
    msg += " priority=" + "\"" +  CCM_priority + "\">"
    msg += str(val) + "</DATA>"
    msg += "<IP>" + CCM_ip + "</IP></UECS>"
    print(msg)
    s.sendto(msg, (ADDRESS, PORT))
    wdt.feed()

##################################################
# Sensor Function
##################################################
def measure_1wire(timer0):
  ds_sensor.convert_temp()
  for rom in roms:
    sleep_ms(750)
    try:
      if ONEWIRE_Multiple:
        onewire_id_list=[]
        onewire_id = str(rom).replace("bytearray(b\'(","").replace("\')","") 
        serial='_'
        onewire_id_list=onewire_id.split("\\x")
        for i in onewire_id_list:
          serial=serial+str(i)
      else: 
        serial =""
      print('< One Wire Serial No: >')
      print(serial)
      d=ds_sensor.read_temp(rom)
      CCM_send(serial,d)
    except:
      print('read error')
      continue

def measure_sht31(timer1):
  try:
    measure_data = sht31.read_temp_humd(False)  #measure_data[0] #temp,measure_data[1] #hum
  except:
    print('sht31 read error')

  if len(measure_data)>0:
    tmp=round(measure_data[0],2)
    hum=round(measure_data[1],2)
  #   housa measure_data[2]
    if tmp>0 and hum>0: 
      hd = math.pow(6.1078*10,((7.5*tmp/(tmp+237.3))))
      hd = 217*hd/(tmp+273.15)
      hd = (100-hum)*hd/100.0
    else:
      hd =""
    # roten measure_data[3]
    if tmp>0 and hum>0:
      if hum<=99.99: #20211025
        dp = math.pow(6.1078*10,((7.5*tmp/(tmp+237.3))))
        dp =(dp*hum)/100.0
        dp = (237.3* math.log10(6.1078/dp))/(math.log10(dp/6.1078)-7.5)
      else:
        dp = tmp
    else:
      dp = ""

    # Absolute humidity measure_data[4] 20210917
    if tmp>0 and hum>0:
      vh = math.pow(6.1078*10,((7.5*tmp/(tmp+237.3))))
      vh = (217*vh)/(tmp+273.15)
      vh = vh * hum /100.0
    else:
      vh = ""

    d = {'_sht31temp': tmp, '_sht31hum': hum, '_sht31hd':hd ,'_sht31dp':dp,'_sht31vh':vh}

    for k, v in d.items():
      CCM_send(k,v)
      sleep_ms(100) #20211025

def get_hx711(timer2):
  val = hx.get_units(10)
  if json_load['HX711']['raw_data_send']:
    CCM_send(hx711_name,val)
    print("HX711_raw_value : %.1f" % (val))
  else:
    slope = json_load['HX711']['slope']
    intercept = json_load['HX711']['y_intercept']
    #g = 0.061258206 * val -94.19149892 
    g = slope * val + intercept
    CCM_send(hx711_name,g)    
    print("HX711_value : %.1f" % (g))

##################################################
# main esp32_UECS 
##################################################
if json_load['ONEWIRE']['load']:
  print("<< mesure 1wire >>")
  timer0.init(period=ms0, mode = machine.Timer.PERIODIC, callback = measure_1wire)

if json_load['SHT31']['load']:
  print("<< mesure sht31 >>")
  timer1.init(period=ms1, mode = machine.Timer.PERIODIC, callback = measure_sht31)

if json_load['HX711']['load']:
#  get_hx711(DOUT_PIN,SCK_PIN,hx711_name)
  print("<< mesure hx711 >>")
  timer2.init(period=ms2, mode = machine.Timer.PERIODIC, callback = get_hx711)

  #  sleep for 30 seconds (30000 milliseconds)
#  machine.deepsleep(30000)
