DS18x20 temperature sensor driver for MicroPython.
MIT license; Copyright (c) 2016 Damien P. George

1-Wire driver for MicroPython
MIT license; Copyright (c) 2016 Damien P. George

sht31 driver is depend on Michael Li.
https://github.com/mikelisfbay/ESP32THING_MicroPython_SHT31_I2C

hx711 driver is depend on robert-hh. 
https://github.com/robert-hh/hx711-lopy

other
esp32_UECS is MIT License Copyright (c) 2020 ookuma yousuke


