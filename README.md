# README #
 UECS UDP Broadcast Program  
 UECSとは、ユビキタス環境制御システム(Ubiquitous Environment Control System)の頭文字を表し、「ウエックス」と読みます。植物を生産するためのガラス室・ハウス(温室)、植物工場などの園芸施設の環境制御を実現するための優れた自律分散型システムです。 そのUECSをESP32上でMicropythonで実行します。
 https://uecs.jp/uecs/index.html
 
【Support Sensor】  
  1wire(DS18B20) … 気温のみ UECS CCM送信します。  
  [sht31](http://akizukidenshi.com/catalog/g/gK-12125/)  … 気温、相対湿度、飽差、露点温度、絶対湿度 を UECS CCM送信します。  
  [hx711](http://akizukidenshi.com/catalog/g/gK-12370/)  … ロードセルアンプ対応。計測した重さを UECS CCM送信します。  
 
 【Support board】  
 ESP32 
 
 【programming language】  
 Micropython  
 
### 1.概要 ###
https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/browse/  

### 2.材料 ###
https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/%E6%9D%90%E6%96%99  

### 3.配線図 ###
https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/%E9%85%8D%E7%B7%9A%E5%9B%B3   

### 4.micropythonリファレンス ###
https://micropython-docs-ja.readthedocs.io/ja/latest/esp32/tutorial/intro.html  

### 5.micropython Firmware ダウンロード ###
動作確認済みFirmware：[GENERIC : esp32-20210902-v1.17.bin](https://micropython.org/resources/firmware/esp32-20210902-v1.17.bin)  
Firmware一覧 https://micropython.org/download/  

### 6.パラメータ（esp32_uecs.json）の設定 ###
  当プログラムファイルをダンロードしてください。  
  その後、設定ファイルを編集します。下記のリンクを確認してください。  
  https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/%E8%A8%AD%E5%AE%9A  

### 7.セットアップ手順 ###
* ボード初期化  
    `$ esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash` 
  
* Micropython 書込み  
    `$ esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-20210902-v1.17.bin`  
  
* /dev/ttyUSB0に接続する  
    `$ rshell -p /dev/ttyUSB0`  
    rshellが使えない場合、  
    [rshell (https://github.com/dhylands/rshell)](https://github.com/dhylands/rshell)をインストールもしくは  
    [ampy (https://github.com/pycampers/ampy)](https://github.com/pycampers/ampy)を使用ください  
      
```  
※＜参考＞  
rshell で接続して、pyboard/main.pyをnano エディタで編集することができます。  
  $ rshell -p /dev/ttyUSB0  -e nano  
  $ edit /pyboard/main.py  
```  
    
* プログラム書き込み  
    解凍したプログラムをPyboardに流します。  
    `$ rsync . /pyboard`  
```      
    失敗する場合は、以下のコマンドで1つ1つ追加してください。  
    $ cp boot.py /pyboard  
    $ cp ds18x20.py /pyboard  
    $ cp hx711.py /pyboard  
    $ cp main.py /pyboard  
    $ cp onewire.py /pyboard  
    $ cp sht31.py /pyboard  
    $ cp esp32_uecs.json /pyboard  
```  

* 動作確認  
 ・[UECSパッケトキャプチャソフト](https://uecs.org/arduino/uecsrs.html)を利用して、該当IPアドレスからUECS UDP ブロードキャストを受信していることを確認してください  
 ・シリアル通信確認ソフト  
   [gtkterm](https://github.com/Jeija/gtkterm)  
 ・シリアル通信 コマンド  
  `$ screen /dev/ttyUSB0 115200`  
 

### 動画手順 ###
  https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/%E5%8B%95%E7%94%BB%E6%89%8B%E9%A0%86  
    

### 関連ソフトウェア ###
  https://bitbucket.org/bigbearfarm/esp32_uecs/wiki/%E5%90%84%E7%A8%AESoftWare%E7%AD%89   


### 注意事項 ###
  このツールを利用したことによって生じたいかなる結果についても責任を負うことはありませんので自己責任の上で使用して下さい。
