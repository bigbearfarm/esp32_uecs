import machine,network,ujson
# json config file read 
json_open = open('esp32_uecs.json', 'r')
json_load = ujson.load(json_open)
ip_address = json_load['CCM']['ip_address']
ssid = json_load['WIFI']['ssid']
password = json_load['WIFI']['pass']
router_ip = json_load['WIFI']['router_ip']

# connect to wifi
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.ifconfig((ip_address, "255.255.255.0", router_ip, router_ip))
sta_if.connect( ssid , password )

while not sta_if.isconnected():
    pass
print(sta_if.ifconfig())
# enable wake on WLAN
#wlan.irq(trigger=WLAN.ANY_EVENT, wake=machine.SLEEP)
# go to sleep
#machine.lightsleep()
